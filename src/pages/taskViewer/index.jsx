import { inject, observer } from "mobx-react";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import cn from "classnames";
import { Link } from "react-router-dom";

import styles from "./index.module.css";

const TaskViewer = inject("TasksStore")(
  observer(({ TasksStore }) => {
    const { id } = useParams();
    const { fetchTaskById, fetchedTask } = TasksStore;

    useEffect(() => {
      fetchTaskById(id);
    }, [fetchTaskById]);

    if (!fetchedTask) {
      return <span>Нет данных</span>;
    }

    const { deadline } = fetchedTask;
    const expired = Date.now() > Date.parse(deadline);
    let taskStatus;
    if (fetchedTask.done) {
      taskStatus = "Выполнена";
    } else {
      taskStatus = expired ? "Просрочена" : "В работе";
    }

    const [y, m, d, time] = deadline.split(/[-T]/);
    const deadlineDateTime = `${d}.${m}.${y} ${time}`;

    return (
      <>
        <Link to="/">Назад к списку задач</Link>
        <ul>
          <li>id задачи: {fetchedTask.id}</li>
          <li>Дедлайн: {deadlineDateTime}</li>
          <li>Статус задачи: {taskStatus}</li>
          <li>наименование задачи: {fetchedTask.title}</li>
        </ul>
        <div>Описание задачи:</div>
        <div className={cn(styles.taskDescription)}>
          {fetchedTask.description}
        </div>
      </>
    );
  })
);

export default TaskViewer;
