import { useEffect } from "react";
import { observer, inject } from "mobx-react";
import { Link } from "react-router-dom";
import cn from "classnames";

import TasksContainer from "../../components/tasksContainer";

import styles from "./index.module.css";

const TasksList = inject("TasksStore")(
  observer(({ TasksStore }) => {
    const { tasksToDisplay, fetchTasksList, deleteTaskById } = TasksStore;

    useEffect(() => {
      fetchTasksList();
    }, [fetchTasksList]);

    useEffect(() => {}, []);

    const content =
      tasksToDisplay.length === 0 ? (
        <div>Нет задач...</div>
      ) : (
        <TasksContainer onDeleteTask={deleteTaskById} data={tasksToDisplay} />
      );

    return (
      <>
        <Link className={cn(styles.createTask)} to="/create">
          Создать задачу
        </Link>
        {content}
      </>
    );
  })
);

export default TasksList;
