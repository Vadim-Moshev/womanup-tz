import cn from "classnames";
import { observer, inject } from "mobx-react";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";

import styles from "./index.module.css";

const taskEditor = inject("TasksStore")(
  observer(({ TasksStore }) => {
    const navigate = useNavigate();
    const { id } = useParams();

    const {
      newTask,
      fetchedTask,
      fetchTaskById,
      changeTaskData,
      saveTask,
      reset,
      isEditorModeEnabled,
      toggleEditorMode,
    } = TasksStore;

    useEffect(() => {
      toggleEditorMode(id !== undefined);
      if (isEditorModeEnabled) {
        fetchTaskById(id);
      }
    });

    useEffect(() => {
      return () => reset();
    }, [reset]);

    const submitForm = (event) => {
      event.preventDefault();
      saveTask();
      navigate("/");
    };

    const targetTask = isEditorModeEnabled ? fetchedTask : newTask;
    const comoleteTaskBlock = isEditorModeEnabled && (
      <label>
        <input
          onChange={(event) => {
            changeTaskData(targetTask, "done", event.target.checked);
          }}
          type="checkbox"
          value="Сохранить"
          checked={targetTask.done}
        />
        <span>Задача выполнена</span>
      </label>
    );

    const header = isEditorModeEnabled ? "Изменить заявку" : "Создать заявку";

    return (
      <>
        <div className={cn(styles.taskEditorHeader)}>{header}</div>
        <form onSubmit={submitForm} className={cn(styles.taskEditorForm)}>
          {comoleteTaskBlock}
          <span>Наименование</span>
          <input
            autoFocus={true}
            type="text"
            value={targetTask.title}
            onChange={(event) => {
              changeTaskData(targetTask, "title", event.target.value);
            }}
            required
          />
          <span>Описание:</span>
          <textarea
            rows="5"
            value={targetTask.description}
            onChange={(event) => {
              changeTaskData(targetTask, "description", event.target.value);
            }}
            required
          />
          <span>Дедлайн:</span>
          <input
            type="datetime-local"
            value={targetTask.deadline}
            onChange={(event) => {
              changeTaskData(targetTask, "deadline", event.target.value);
            }}
            required
          />
          <div className={cn(styles.taskEditorButtons)}>
            <input type="submit" value="Сохранить" />
          </div>
        </form>
      </>
    );
  })
);

export default taskEditor;
