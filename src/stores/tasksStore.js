import { makeObservable, observable, set, action } from "mobx";

const TASK_STRUCTURE = {
  id: "",
  title: "",
  done: false,
  deadline: "2005-06-01T09:00",
  description: "",
};

/**
 * Описывает стор для хранения задач и методов для работы с ними.
 * @constructor
 */
class TasksStore {
  tasksToDisplay = [];
  tasksStorage = [];
  fetchedTask = {};
  newTask = {};
  isEditorModeEnabled = false;

  constructor() {
    this.reset();

    makeObservable(this, {
      tasksToDisplay: observable,
      fetchedTask: observable,
      newTask: observable,
      isEditorModeEnabled: observable,

      fetchTasksList: action,
      fetchTaskById: action,
      reset: action,
      saveTask: action,
      toggleEditorMode: action,
      deleteTaskById: action,
    });
  }

  /**
   * Сброс полей формы добавления / редактирования задачи
   * в состояние по умолчанию, очищая их от предыдущих значений.
   * Подготавливает редактор задач к созданию
   * новой задачи или редактированию выбранной.
   * @method
   */
  reset = () => {
    this.newTask = { ...TASK_STRUCTURE };
    this.fetchedTask = { ...TASK_STRUCTURE };
  };

  /**
   * Получает список задач для отображения на главной странице
   * @method
   */
  fetchTasksList = () => {
    this.tasksToDisplay = this.tasksStorage;
  };

  /**
   * Получает одну задачу по её идентификатору, переданного через аргумент.
   * @method
   * @param {string} id - Идентификатор задачи
   */
  fetchTaskById = (id) => {
    this.fetchedTask = this.tasksToDisplay.find((task) => id === task.id);
  };

  /**
   * Записывает информацию, переданную через value в поле field объекта object.
   * Вызывается при изменении какого-либо поля в задаче.
   * @method
   * @param {object} object - Идентификатор задачи
   * @param {string} field - Идентификатор задачи
   * @param {any} value - Идентификатор задачи
   */
  changeTaskData = (object, field, value) => {
    set(object, field, value);
  };

  /**
   * Переключает редактор задач между режимами редактирования и создания новой задачи.
   * Переключение происходит посредством передачи аргумента flag. Значение true этого аргумента
   * переключает редактор в режим редактирования задач, а false — в режим создания новой задачи.
   * @method
   * @param {boolean} flag - признак активности режима редактирования задачи
   */
  toggleEditorMode = (flag) => {
    this.isEditorModeEnabled = flag;
  };

  /**
   * Если редактор находится в режиме создания задачи, метод создаёт новую задачу.
   * Если редактор находится в режиме редактирования задачи, метод изменяет задачу
   * с идентификатором taskId
   * @method
   * @param {string} taskId - Идентификатор задачи
   */
  saveTask = (taskId) => {
    if (this.isEditorModeEnabled) {
      const editingTaskIndex = this.tasksStorage.findIndex(
        (task) => task.id === taskId
      );
      this.tasksStorage[editingTaskIndex] = this.fetchedTask;
      return;
    }

    const newTaskId = new Date().getTime().toString();
    this.newTask.id = newTaskId;
    this.tasksStorage.push(this.newTask);
  };

  /**
   * Метод удаляет задачу с идентификатором taskId
   * @method
   * @param {string} taskId - Идентификатор задачи
   */
  deleteTaskById = (taskId) => {
    const index = this.tasksStorage.findIndex((task) => task.id === taskId);
    this.tasksStorage.splice(index, 1);
    this.tasksToDisplay = [...this.tasksStorage];
  };
}

export default new TasksStore();
