import { createBrowserHistory } from "history";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Provider } from "mobx-react";
import { syncHistoryWithStore } from "mobx-react-router";

import TasksList from "./pages/tasksList";
import TaskEditor from "./pages/taskEditor";
import TaskViewer from "./pages/taskViewer";

import stores from "./stores";
import routing from "./stores/routing";

import "./App.css";

const browserHistory = createBrowserHistory();
const history = syncHistoryWithStore(browserHistory, routing);

function App() {
  return (
    <Provider {...stores}>
      <BrowserRouter history={history}>
        <div className="wrapper">
          <Routes>
            <Route exact path="/" element={<TasksList />} />

            <Route exact path="/edit/:id" element={<TaskEditor />} />
            <Route exact path="/create" element={<TaskEditor />} />

            <Route exact path="/view/:id" element={<TaskViewer />} />
          </Routes>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
