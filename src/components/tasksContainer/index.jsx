import cn from "classnames";

import TaskItem from "../taskItem";

import styles from "./index.module.css";

const TasksContainer = ({ data, onDeleteTask }) => {
  const tasksItems = data.map((task) => {
    return (
      <TaskItem onDeleteTask={onDeleteTask} key={task.id} taskFields={task} />
    );
  });

  return <div className={cn(styles.tasksContainer)}>{tasksItems}</div>;
};

export default TasksContainer;
