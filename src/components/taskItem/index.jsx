import cn from "classnames";
import { Link } from "react-router-dom";

import styles from "./index.module.css";

const TaskItem = ({ taskFields, onDeleteTask }) => {
  const { id, title, done, deadline } = taskFields;

  const expired = Date.now() > Date.parse(deadline);

  const borderStyles = cn(
    styles.taskItem,
    { [styles.taskItem__expired]: expired },
    { [styles.taskItem__done]: done }
  );

  const titleStyles = cn(
    { [styles.taskItem_title__expired]: expired },
    { [styles.taskItem_title__done]: done }
  );

  return (
    <div className={borderStyles}>
      <Link className={titleStyles} to={`/view/${id}`}>
        {title}
      </Link>

      <div className={cn(styles.taskItem_controls)}>
        <Link to={`/edit/${id}`}>Редактировать</Link>
        <button
          onClick={() => {
            onDeleteTask(id);
          }}
        >
          Удалить
        </button>
      </div>
    </div>
  );
};

export default TaskItem;
